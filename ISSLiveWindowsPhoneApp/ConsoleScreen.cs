using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Newtonsoft.Json.Linq;

namespace ISSLiveWindowsPhoneApp
{
    public class ConsoleScreen
    {
        public ConsoleScreen(KeyValuePair<string, JArray> consoleScreen)
        {
            if(consoleScreen.Key==null ) return;

            ConsoleScreenName = consoleScreen.Key;

            SensorInfos = new ObservableCollection<SensorInfo>();

            foreach (var sensorInfo in consoleScreen.Value.Select(
                sensorInfoJtoken => new SensorInfo(sensorInfoJtoken)))
            {
                SensorInfos.Add(sensorInfo);
            }
        }

        public string ConsoleScreenName { get; set; }

        public ObservableCollection<SensorInfo> SensorInfos { get; set; }
    }
}