﻿using System.Windows.Controls;
using Microsoft.Phone.Controls;

namespace ISSLiveWindowsPhoneApp
{
    public partial class ConsoleScreenPage : PhoneApplicationPage
    {
        public ConsoleScreenPage()
        {
            InitializeComponent();
            DataContext = App.CurrentConsoleScreen;
            CalibratedValueUpdateProcess.Start();
        }

        private void TextBlockTap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            var tb = sender as TextBlock;
            var text = tb.Text;
        }

        private void StackPanel_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            var sp = sender as StackPanel;
            var dc = sp.DataContext as SensorInfo;
        }

        protected override void OnNavigatedFrom(System.Windows.Navigation.NavigationEventArgs e)
        {
            CalibratedValueUpdateProcess.End();
            base.OnNavigatedFrom(e);
        }
    }

    public class SensorLiveData
    {
        
    }
}