namespace ISSLiveWindowsPhoneApp
{
    public static class Extensions
    {
        public static string ToStringEx(this object obj)
        {
            return obj == null ? null : obj.ToString();
        }

        public static int ToIntEx(this object obj)
        {
            if (obj == null) return 0;
            int value;
            int.TryParse(obj.ToString(), out value);
            return value;
        }
    }
}