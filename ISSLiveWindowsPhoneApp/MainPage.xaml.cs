﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net;
using System.Windows;
using Microsoft.Phone.Controls;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace ISSLiveWindowsPhoneApp
{
    public partial class MainPage : PhoneApplicationPage
    {
        // Constructor
        public MainPage()
        {
            InitializeComponent();
            GetMetaData();
        }

        private HttpWebRequest webRequest;
        private void GetMetaData()
        {
            var client = new WebClient();
            client.DownloadStringCompleted += ClientDownloadStringCompleted;
            client.DownloadStringAsync(new Uri(App.AppSettings.ServerUrl+":8000/api/metadata"));
        }


        void ClientDownloadStringCompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            try
            {
                App.MetaData = e.Result;
                var consoleMap = JsonConvert.DeserializeObject<Dictionary<string, Dictionary<String, JArray>>>(e.Result);

                var consoles = new ObservableCollection<Console>();
                foreach (var console in consoleMap)
                {
                    consoles.Add(new Console(console));
                }
                App.AppSettings.ConsoleMetaData = consoles;

                var groupConsoles = new ObservableCollection<GroupConsole>();
                foreach (var console in consoles)
                {
                    groupConsoles.Add(new GroupConsole(console));
                }

                App.GroupConsoles = groupConsoles;

                Dispatcher.BeginInvoke(() => NavigationService.Navigate(new Uri("/ConsolePage.xaml", UriKind.Relative)));
            }
            catch (WebException exception)
            {
                MessageBox.Show("Server not found");
            }
        }

    }
}