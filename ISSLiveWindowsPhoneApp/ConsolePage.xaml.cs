﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace ISSLiveWindowsPhoneApp
{
    public partial class ConsolePage
    {
        public ConsolePage()
        {
            InitializeComponent();


            DataContext = App.Consoles;
        }

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            if (NavigationService.CanGoBack)
                NavigationService.RemoveBackEntry();

            base.OnNavigatedTo(e);
        }
        private void BusinessesListBoxSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var listBox = sender as ListBox;
            App.CurrentConsoleScreen = listBox.SelectedItem as ConsoleScreen;
            App.CurrentConsoleName = (PivotControl.SelectedItem as Console).ConsoleName;
            NavigationService.Navigate(new Uri("/ConsoleScreenPage.xaml", UriKind.Relative));
        }
    }

}