using System.Collections.Generic;
using System.Collections.ObjectModel;
using Newtonsoft.Json.Linq;

namespace ISSLiveWindowsPhoneApp
{
    public class Console
    {
        public Console(KeyValuePair<string, Dictionary<string, JArray>> console)
        {
            if(console.Key==null) return;

            ConsoleName = console.Key;
            ConsoleScreens = new ObservableCollection<ConsoleScreen>();
            foreach (var consoleScreen in console.Value)
            {
                ConsoleScreens.Add(new ConsoleScreen(consoleScreen));
            }
        }

        public string ConsoleName { get; set; }

        public ObservableCollection<ConsoleScreen> ConsoleScreens { get; set; }


    }
}