﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;

namespace ISSLiveWindowsPhoneApp
{
    public partial class Loading : PhoneApplicationPage
    {
        public Loading()
        {
            InitializeComponent();

            Loaded += new RoutedEventHandler(Loading_Loaded);
        }

        void Loading_Loaded(object sender, RoutedEventArgs e)
        {
            if (App.AppSettings.ConsoleMetaData == null || App.AppSettings.ConsoleMetaData.Count == 0)
                NavigationService.Navigate(new Uri("/MainPage.xaml", UriKind.Relative));
            else
                NavigationService.Navigate(new Uri("/ConsolePage.xaml", UriKind.Relative));
        }
    }
}