namespace ISSLiveWindowsPhoneApp
{
    public static class ApiResponseKeys
    {
        public const string ShortDescription = "shortDesc";
        public const string SensorName = "name";
        public const string Index = "index";
        public const string AliasSmall = "alias";
        public const string Description = "desc";

        public const string Name = "Name";
        public const string Value = "Value";
        public const string CalibratedData = "CalibratedData";
        public const string LastUpdated = "LastUpdated";
        public const string Alias = "Alias";


    }
}