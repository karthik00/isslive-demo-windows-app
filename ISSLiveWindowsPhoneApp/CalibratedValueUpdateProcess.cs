using System;
using System.Net;
using System.Windows.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace ISSLiveWindowsPhoneApp
{
    public static class CalibratedValueUpdateProcess
    {
        private static readonly DispatcherTimer Timer = new DispatcherTimer();
        public static void Start()
        {
            DownloadCalibratedValues();
        }

        static void TimerTick(object sender, EventArgs e)
        {
            Timer.Tick -= TimerTick;

            DownloadCalibratedValues();
        }

        private static void DownloadCalibratedValues()
        {
            var webClient = new WebClient();
            webClient.DownloadStringCompleted += WebClientDownloadStringCompleted;
            webClient.DownloadStringAsync(new Uri(App.AppSettings.ServerUrl+":8000/api/"+App.CurrentConsoleName+"/"+App.CurrentConsoleScreen.ConsoleScreenName));
        }
        
        private static int count = 0;
        static void WebClientDownloadStringCompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            if (!Timer.IsEnabled) return;

            try
            {
                var result = e.Result;

                var sensorValues = (JArray)JsonConvert.DeserializeObject(result);

                count = 0;
                foreach (var sensorValue in sensorValues)
                {
                    var sensorName = sensorValue[ApiResponseKeys.Name].ToStringEx();
                    if (sensorName == null)
                        continue;

                    foreach (var info in App.CurrentConsoleScreen.SensorInfos)
                    {
                        if(info.SensorName.Equals(sensorName))
                        {
                            info.CalibratedValue = sensorValue[ApiResponseKeys.CalibratedData].ToStringEx();
                            info.Value = sensorValue[ApiResponseKeys.Value].ToStringEx();
                            break;
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                //blind retry.
                if(count++<2)
                    DownloadCalibratedValues();
            }

            Timer.Interval = new TimeSpan(0, 0, 0, 15);
            Timer.Tick += TimerTick;
            Timer.Start();
        }

        public static void End()
        {
            Timer.Stop();
        }
    }
}