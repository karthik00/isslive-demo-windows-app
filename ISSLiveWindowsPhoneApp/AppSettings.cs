﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO.IsolatedStorage;
using System.Net;
using Newtonsoft.Json;

namespace ISSLiveWindowsPhoneApp
{
    public class AppSettings
    {
        // Our isolated storage settings
        private readonly IsolatedStorageSettings _settings;

        private const string ConsoleMetaDataKeyName = "ConsoleMetaData";
    
        //I don't want to create a new instance every time
        //So internally I am maintaining an instance.
        //I can make this class a singleton, but there is binding associated to a StaticResource AppSettings
        //Din't want to mess up with the design practice msdn suggests.
        private static AppSettings _instance;

        public static AppSettings Instance
        {
            get { return _instance ?? (_instance = new AppSettings()); }
        }

        /// <summary>
        /// Constructor that gets the application settings.
        /// </summary>
        public AppSettings()
        {
            if (DesignerProperties.IsInDesignTool)
                return;

            // Get the settings for this application.
            _settings = IsolatedStorageSettings.ApplicationSettings;


            //I removed the condition below because it is just an assignment
            //And this condition adds more weight. :)
            //if (_instance == null)
            _instance = this;
        }


        /// <summary>
        /// Update a setting value for our application. If the setting does not
        /// exist, then add the setting.
        /// </summary>
        /// <param name="Key"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public bool AddOrUpdateValue(String Key, Object value)
        {
            bool valueChanged = false;

            // If the key exists
            if (_settings.Contains(Key))
            {
                // If the value has changed
                if (_settings[Key] != value)
                {
                    // Store the new value
                    _settings[Key] = value;
                    valueChanged = true;
                }
            }
            // Otherwise create the key.
            else
            {
                _settings.Add(Key, value);
                valueChanged = true;
            }
            return valueChanged;
        }

        /// <summary>
        /// Get the current value of the setting, or if it is not found, set the 
        /// setting to the default setting.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="Key"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public T GetValueOrDefault<T>(String Key, T defaultValue)
        {
            T value;

            // If the key exists, retrieve the value.
            if (_settings.Contains(Key))
            {
                value = (T)_settings[Key];
            }
            // Otherwise, use the default value.
            else
            {
                value = defaultValue;
            }

            return value;
        }

        object _lockObject = new object();
        /// <summary>
        /// Save the settings.
        /// </summary>
        public void Save()
        {
            lock (_lockObject)
            {
                _settings.Save();   
            }
        }

        public ObservableCollection<Console> ConsoleMetaData
        {
            get { return string.IsNullOrWhiteSpace(GetValueOrDefault(ConsoleMetaDataKeyName, ""))?null: 
                JsonConvert.DeserializeObject<ObservableCollection<Console>>(GetValueOrDefault(ConsoleMetaDataKeyName, "")); }
            set
            {
                if (AddOrUpdateValue(ConsoleMetaDataKeyName, JsonConvert.SerializeObject(value)))
                {
                    Save();
                }
            }
        }

        private const string ServerUrlKeyName = "ServerUrl";

        public string ServerUrl
        {
            get { return GetValueOrDefault(ServerUrlKeyName, "http://192.168.10.172"); }
            set
            {
                if (AddOrUpdateValue(ServerUrlKeyName, value))
                {
                    Save();
                }
            }
        }
    }
}