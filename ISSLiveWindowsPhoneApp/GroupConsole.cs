using System.Collections.ObjectModel;

namespace ISSLiveWindowsPhoneApp
{
    public class GroupConsole
    {
        public string ConsoleName { get; set; }

        public ObservableCollection<ConsoleScreenGroup<SensorInfo>> ConsoleScreens { get; set; }

        public GroupConsole(Console console)
        {
            ConsoleName = console.ConsoleName;
            ConsoleScreens = new ObservableCollection<ConsoleScreenGroup<SensorInfo>>();
            foreach (var consoleScreen in console.ConsoleScreens)
            {
                ConsoleScreens.Add(new ConsoleScreenGroup<SensorInfo>(consoleScreen.ConsoleScreenName, consoleScreen.SensorInfos));
            }
        }
    }
}