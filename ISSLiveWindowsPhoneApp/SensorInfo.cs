using System.ComponentModel;
using Newtonsoft.Json.Linq;

namespace ISSLiveWindowsPhoneApp
{
    public class SensorInfo : INotifyPropertyChanged
    {
        public string ShortDescription { get; set; }

        public string SensorName { get; set; }

        public int Index { get; set; }

        public string Alias { get; set; }

        public string Description { get; set; }

        private string _calibratedValue;

        public string CalibratedValue
        {
            get { return _calibratedValue; }
            set
            {
                if (_calibratedValue == value) return;

                _calibratedValue = value;
                OnPropertyChanged("CalibratedValue");
            }
        }

        private string _value;

        public string Value
        {
            get { return _value; }
            set
            {
                if (_value == value) return;

                _value = value;
                OnPropertyChanged("Value");
            }
        }

        public SensorInfo() { }

        public SensorInfo(JToken sensorInfoJToken)
        {
            ShortDescription = sensorInfoJToken[ApiResponseKeys.ShortDescription].ToStringEx();

            SensorName = sensorInfoJToken[ApiResponseKeys.SensorName].ToStringEx();

            Index = sensorInfoJToken[ApiResponseKeys.Index].ToIntEx();

            Alias = sensorInfoJToken[ApiResponseKeys.AliasSmall].ToStringEx();

            Description = sensorInfoJToken[ApiResponseKeys.Description].ToStringEx();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}